# ping_display
Displays an ongoing graph of ping to user defined address from a specific time span


# Requirements
Python 3 or something. Install requirements from requirements.txt.
`pip install -r requirements.txt`


# How to use
`python ping_display`
Display ping to google.com from last 5 minutes

`python ping_display my_address.com`
Display ping to my_address.com from last 5 minutes

`python ping_display my_address.com 10`
Display ping to my_address.com from last 10 minutes
