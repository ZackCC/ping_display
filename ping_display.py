import matplotlib.pyplot as plt
import matplotlib.animation as animation
import threading
import time
import sys
import os
import re


class PingDisplay(object):

    def __init__(self, address=None, time_span=None):
        self.data = []
        self.address = address if address else "google.com"
        self.time_span = time_span if time_span else 5*60
        self._ping_started = False
        self._initialize_plot()

    def start_ping(self):
        self._ping_started = True
        ping_thread = threading.Thread(target=self._start_ping)
        ping_thread.start()
        print("Pinging {}".format(self.address))

    def stop_ping(self):
        print("Stopping ping display...")
        self._ping_started = False

    def _start_ping(self):
        while self._ping_started:
            result = os.popen("ping {} -n 1".format(self.address)).read()
            try:
                ping = int(re.search(r"(?<=Average = )\d+(?=ms)", result).group())
            except Exception:
                # Could not get ping for whatever reason
                print("Could not get ping. Result was: {}".format(result))
                ping = 999

            if len(self.data) < self.time_span:     # Data set is less than full time span we want to observe. Just append to it.
                self.data.append(ping)
            else:                                   # Data set is enough to cover required time span. Remove first data point and add a new one.
                self.data.pop(0)
                self.data.append(ping)
            time.sleep(0.5)

    def show_graph(self):
        ani = animation.FuncAnimation(self.fig, self._animate, interval=1000)
        plt.show()

    def _initialize_plot(self):
        self.fig = plt.figure()
        self.ax1 = self.fig.add_subplot(1, 1, 1)

    def _animate(self, i):
        self.ax1.clear()
        self.ax1.title.set_text("Ping to {} from past {} minutes".format(self.address, int(self.time_span / 60)))
        self.ax1.plot(self.data)


# TODO: change to a proper commandline interface or something
def get_commandline_arguments():
    try:
        address = sys.argv[1]
    except Exception:
        address = None

    try:
        time_span = int(sys.argv[2])*60
    except Exception:
        time_span = None
    return address, time_span

address, time_span = get_commandline_arguments()
ping_display = PingDisplay(address, time_span)
ping_display.start_ping()
ping_display.show_graph()
ping_display.stop_ping()
